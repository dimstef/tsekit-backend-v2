module.exports = ({ env }) => ({
  defaultConnection: 'default',
  connections: {
    default: {
      connector: 'bookshelf',
      settings: {
        client: "postgres",
        host: env("DATABASE_HOST_DEVELOPMENT"),
        port: env("DATABASE_HOST_PORT"),
        database: env("DATABASE_HOST_NAME"),
        username: env("DATABASE_HOST_USERNAME"),
        password: env("DATABASE_HOST_PASSWORD")

      },
      options: {
        //useNullAsDefault: true,
      },
    },
  },
});
