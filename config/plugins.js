module.exports = ({ env }) => ({
  email: {
    provider: 'sendgrid',
    providerOptions: {
      apiKey: env('SENDGRID_API_KEY'),
    },
    settings: {
      defaultFrom: 'officialhotspotapp@gmail.com',
      defaultReplyTo: 'officialhotspotapp@gmail.com',
    },
  },
});