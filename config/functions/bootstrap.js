'use strict';

const { default: createStrapi } = require("strapi");

/**
 * An asynchronous bootstrap function that runs before
 * your application gets started.
 *
 * This gives you an opportunity to set up your data model,
 * run jobs, or perform some special logic.
 *
 * See more details here: https://strapi.io/documentation/v3.x/concepts/configurations.html#bootstrap
 */

async function resetCalendar(){
  const calendar = await strapi.query('calendar').delete();
  const days = await strapi.query('day').delete();
  const timestamp = await strapi.query('timestamp').delete();

}
module.exports = async () => {
  const places = await strapi.query('place').find();
  //await resetCalendar();
  for await(let place of places){
    try{
      const calendar = await strapi.query('calendar').create({place: place.id});

      let days = ["monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"];
      let generatedDays = [];
      for await (let _day of days){
        const day = await strapi.query('day').create({day: _day, calendar:calendar.id});
        generatedDays.push(day)
      }
      await strapi.query('calendar').update({id:calendar.id}, {days:generatedDays})
    }catch(e){
      
    }
  }
};
