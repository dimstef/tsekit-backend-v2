module.exports = ({ env }) => ({
  host: env('HOST', '0.0.0.0'),
  port: env.int('PORT', 1337),
  admin: {
    auth: {
      secret: env('ADMIN_JWT_SECRET', '5f8a3bee856517083fe6ecb218b5179a'),
    },
    url: `${env('DOMAIN_URL')}/dashboard`,
  },
});
