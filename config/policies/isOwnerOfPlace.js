'use strict';

/**
 * `isOwnerOfPlace` policy.
 */

module.exports = async (ctx, next) => {
  console.log('In isOwnerOfPlace policy.');

  await next();
};
