'use strict';

/**
 * Read the documentation (https://strapi.io/documentation/v3.x/concepts/models.html#lifecycle-hooks)
 * to customize this model
 */

module.exports = {
  lifecycles:{
    async beforeCreate(data) {
      const timestampAlreadyExists = await strapi.query('timestamp').findOne({ day: data.day, time:data.time});
      if(timestampAlreadyExists){
        throw new Error("already exists");
      }
    },
  }
};
