'use strict';

/**
 * Read the documentation (https://strapi.io/documentation/v3.x/concepts/controllers.html#core-controllers)
 * to customize this controller
 */
const { sanitizeEntity } = require('strapi-utils');

async function getPlace(ctx){
  const { id } = ctx.params;
  let _day = ctx.request.body.day;
  const day = await strapi.services.day.findOne({id:_day});
  const place = await strapi.services.place.findOne({
    id: day.calendar.place,
    'user.id': ctx.state.user.id
  })
  return place;
}

module.exports = {
  async update(ctx){
    const { id } = ctx.params;
    const place = await getPlace(ctx);
    if (!place) {
      return ctx.unauthorized(`You can't update this entry`);
    }
    let entity = await strapi.services.timestamp.update({ id }, ctx.request.body);
    return sanitizeEntity(entity, { model: strapi.models.timestamp });
  },
  async create(ctx){
    const place = await getPlace(ctx);
    if (!place) {
      return ctx.unauthorized(`You can't create this entry`);
    }
    let entity = await strapi.services.timestamp.create(ctx.request.body);
    return sanitizeEntity(entity, { model: strapi.models.timestamp });
  }
};
