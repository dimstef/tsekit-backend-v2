'use strict';

/**
 * Read the documentation (https://strapi.io/documentation/v3.x/concepts/models.html#lifecycle-hooks)
 * to customize this model
 */

module.exports = {
  lifecycles: {
    async afterUpdate(data) {
      const dayArr = data.days.map(function(item){ return item.day });
      const isDuplicate = dayArr.some(function(item, idx){ 
          return dayArr.indexOf(item) != idx 
      });
      if(isDuplicate){
        throw new Error('Duplicate days in calendars are not allowed');  
      }
    },
    async beforeCreate(data){
      const calendarAlreadyExists = await strapi.query('calendar').findOne({ place: data.place });
      if(calendarAlreadyExists){
        throw new Error('Duplicate calendars are not allowed');        
      }
    }
  },
};
