'use strict';

/**
 * Read the documentation (https://strapi.io/documentation/v3.x/concepts/controllers.html#core-controllers)
 * to customize this controller
 */

const { sanitizeEntity } = require('strapi-utils');

async function getTimestamps(calendar){
  for await (const [i, day] of calendar.days.entries()){
    const results = await strapi.query('timestamp').find(({ "day": day.id }))

    // no need to get related days since we already have them in "calendar.days"
    results.map((timestamp, i)=>{
      delete results[i].day;
      delete results[i].created_by;
      delete results[i].updated_by;
    })

    // add timestamps to the current day
    try{
      calendar.days[i].timestamps = results;
    }catch(e){
      calendar.days.timestamps = [];
    }
  }
  return calendar
}

module.exports = {
  async find(ctx){
    let entities;
    if (ctx.query._q) {
      entities = await strapi.services.calendar.search(ctx.query);
    } else {
      entities = await strapi.services.calendar.find(ctx.query)
    }

    const unresolvedPromises = Promise.all(entities.map(async entity=>{
      let calendar = sanitizeEntity(entity, {
        model: strapi.models.calendar
      })
      calendar = await getTimestamps(calendar);
      return calendar;
    }))
    return unresolvedPromises;
  },
  async findOne(ctx){
    let entity = await strapi.services.calendar.findOne(ctx.query)
    let calendar = sanitizeEntity(entity, {
      model: strapi.models.calendar
    })
    calendar = await getTimestamps(calendar);
    return calendar;
  }
};
